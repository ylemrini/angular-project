import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css'],
})
export class AppPageComponent implements OnInit {
  name = 'Youssef';

  constructor() {}

  ngOnInit(): void {}
}
