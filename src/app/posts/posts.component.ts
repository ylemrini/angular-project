import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit {
  posts = null;

  constructor(private http: HttpClient) {}

  getPosts() {
    this.http
      .get('https://jsonplaceholder.typicode.com/posts')
      .subscribe((data) => {
        this.posts = data;
      });
  }
  getPost(id) {
    this.http
      .get('https://jsonplaceholder.typicode.com/posts/' + id)
      .subscribe((data) => {
        return data;
      });
  }
  showPost(id) {
    let post = this.getPost(id);
  }
  ngOnInit() {
    this.getPosts();
  }
}
