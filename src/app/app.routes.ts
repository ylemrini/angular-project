import { HomeComponent } from './home/home.component';
import { PostsComponent } from './posts/posts.component';
import { CounterComponent } from './counter/counter.component';

export const APP_ROUTES = [
  { path: '', component: HomeComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'counter', component: CounterComponent },
];
