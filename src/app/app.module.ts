import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AppHeaderComponent } from './header/index';
import { AppPageComponent } from './page/index';
import { AppFooterComponent } from './footer/index';
import { MenuComponent } from './menu/menu.component';
import { CounterComponent } from './counter/counter.component';
import { HomeComponent } from './home/home.component';
import { PostsComponent } from './posts/posts.component';
import { APP_ROUTES } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    AppPageComponent,
    AppFooterComponent,
    MenuComponent,
    CounterComponent,
    HomeComponent,
    PostsComponent,
  ],
  imports: [
    RouterModule.forRoot(APP_ROUTES),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
